#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    this->get(i)=value;
    while (i>0 && this->get(i) > this->get((i-1)/2)) {
        this->swap(i, (i-1)/2);
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
    int nodeValue = this->get(nodeIndex);
    int leftIndex = this->leftChild(i_max);
    int rightIndex = this->rightChild(i_max);


    if (leftIndex < heapSize) {
        int leftValue = this->get(leftIndex);
        if (leftValue > nodeValue) {
            i_max = leftIndex;
        }
    }
    if (rightIndex < heapSize) {
        int rightValue = this->get(rightIndex);
        if (rightValue > this->get(i_max)) {
            i_max = rightIndex;
        }
    }
    if (i_max!=nodeIndex) {
        this->swap(i_max, nodeIndex);
        heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    int length = numbers.size();
    for (int i=0;i<length;i++) {
        insertHeapNode(this->size(),numbers[i]);
    }
}

void Heap::heapSort()
{
    for (int i=(this->size()-1);i>=0;i--) {
        this->swap(0,i);
        heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
