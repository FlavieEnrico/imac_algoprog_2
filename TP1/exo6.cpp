#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    Noeud* dernier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int size;
    int nbElem;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier=nullptr;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier == nullptr) {
        return true;
    } else {
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
     Noeud *new_noeud=new Noeud;
     new_noeud->donnee=valeur;
     new_noeud->suivant=nullptr;
     if (liste->premier==nullptr) {
         liste->premier=new_noeud;
         liste->dernier=new_noeud;
     } else {
        liste->dernier->suivant=new_noeud;
        liste->dernier=new_noeud;
     }
}

void affiche(const Liste* liste)
{
    Noeud *premier_element=liste->premier;
    while (premier_element!=nullptr) {
        cout << premier_element->donnee << endl;
        premier_element=premier_element->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    Noeud* noeud = liste->premier;
        while (n !=0) {
            if (noeud->suivant != nullptr){
                noeud = noeud->suivant;
                n--;
            }
            else {
                return -1;
            }
        }
    return noeud->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* noeud = liste->premier;
    int place=0;
        while (noeud != nullptr){
            place++;
            if (noeud->donnee == valeur){
                return place;
            }
            noeud = noeud->suivant;
        }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* noeud = liste->premier;
    int i = 1;
    while (n!=i){
        noeud = noeud->suivant;
        i++;
    }
    noeud->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    int* donnees = tableau->donnees;
    if(tableau->nbElem<tableau->size){
        donnees[tableau->nbElem]= valeur;
        tableau->nbElem++;
    }
    else{
        int*tab2 = (int*) malloc (sizeof(int)*tableau->nbElem+1);
        for (int i=0; i<tableau->nbElem; i++){
            tab2[i] = donnees[i];
        }
        tab2[tableau->nbElem] = valeur;
        tableau->nbElem++;
        tableau->donnees = tab2;
    }
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->size=capacite;
    tableau->nbElem = 0;
    tableau->donnees = (int*) malloc (sizeof(int)*tableau->size);
}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->nbElem == 0){
        return true;
    }
    return false;
}

void affiche(const DynaTableau* tableau)
{
    int*donnees = tableau->donnees;
    for(int i =0; i<tableau->nbElem; i++){
        cout << donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    int valeur =tableau->donnees[n];
    return valeur;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int*donnees = tableau->donnees;
    for(int i=0; i<tableau->nbElem; i++){
        if(donnees[i] == valeur){
            return i+1;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud *file = liste->premier;
    if (file == nullptr){
        Noeud *add_file = new Noeud;
        if (add_file == nullptr){
            exit(3);
        }
        add_file->donnee = valeur;
        add_file->suivant = nullptr;
        liste->premier = add_file;
        return;
    }
    while (file->suivant != nullptr) {
        file = file->suivant;
    }
    Noeud *add_file = new Noeud;
    if (add_file == nullptr){
        exit(3);
    }
    add_file->donnee = valeur;
    add_file->suivant = nullptr;
    file->suivant = add_file;
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud *premier = liste->premier;
    if (est_vide(liste)){
        return 1;
    }
    if (premier->suivant == nullptr){
        initialise(liste);
        return 0;
    }
    liste->premier = premier->suivant;
    return premier->donnee;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *pile = liste->premier;
    if (pile == nullptr){
        Noeud *add_pile = new Noeud;
        if (add_pile == nullptr){
                    exit(3);
        }
        add_pile->donnee = valeur;
        add_pile->suivant = nullptr;
        liste->premier = add_pile;
        return;
    }
    Noeud *add_pile = new Noeud;
    if (add_pile == nullptr){
                exit(3);
    }
    add_pile->donnee = valeur;
    add_pile->suivant = pile;
    liste->premier = add_pile;

}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud *premier = liste->premier;
    if (est_vide(liste)){
        return 1;
    }

    if (premier->suivant == nullptr){
        initialise(liste);
        return 0;
    }
    liste->premier = premier->suivant;
    return premier->donnee;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    std::cout << "Elements initiaux tableau:" << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    std::cout << "Elements après stockage de 7 tableau:" << std::endl;
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là... 1" << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là... 2" << std::endl;
    }

    return 0;
}
