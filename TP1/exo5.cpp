#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot

        // check n
        Point znp1;
        znp1.x = (z.x*z.x)-(z.y*z.y)+point.x;
        znp1.y = 2*(z.x*z.y)+point.y;

        if (n==0){
            znp1.x = 0;
            znp1.y = 0;
        }

        // check length of z
        if (znp1.length()>2){
            return n;
        }
        // if Mandelbrot, return 1 or n (check the difference)
        if (n <= 0) {
            return n;
        }

        // otherwise, process the square of z and recall
        // isMandebrot
        return isMandelbrot(znp1,n-1,point);

        /* z = x+iy
         * z^2 = (x*x-y*y)+i(2xy) */
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



