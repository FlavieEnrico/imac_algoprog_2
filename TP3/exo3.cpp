#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value=value;
        left=nullptr;
        right=nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if (this->value > value) {
            if (left != nullptr) {
                left->insertNumber(value);
            }
            else {
                left=new SearchTreeNode(value);
            }
        }
        else {
            if (right!=nullptr) {
                right->insertNumber(value);
            }
            else {
                right=new SearchTreeNode(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->isLeaf()) {
            return 1;
        }
        uint countLeft=0;
        uint countRight=0;

        if (left != nullptr) {
            countLeft = left->height();
        }
        if (right !=nullptr) {
            countRight = right->height();
        }
        if (countLeft < countRight) {
            return countRight+1;
        }
        return countLeft+1;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->isLeaf()) {
            return 1;
        }
        uint totalNodes=0;
        if (left != nullptr) {
            totalNodes += left->nodesCount();
        }
        if (right !=nullptr) {
            totalNodes += right->nodesCount();
        }
        return totalNodes+1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if (left==nullptr && right ==nullptr) {
            return true;
        }
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if (this->isLeaf()) {
            leaves[leavesCount] = this;
            leavesCount++;
        }
        if (left != nullptr) {
            left->allLeaves(leaves, leavesCount);
        }
        if (right !=nullptr) {
            right->allLeaves(leaves, leavesCount);
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if (left != nullptr) {
            left->inorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
        if (right !=nullptr) {
            right->inorderTravel(nodes, nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount++;
        if (left != nullptr) {
            left->preorderTravel(nodes, nodesCount);
        }
        if (right !=nullptr) {
            right->preorderTravel(nodes, nodesCount);
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if (left != nullptr) {
            left->postorderTravel(nodes, nodesCount);
        }
        if (right !=nullptr) {
            right->postorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
	}

	Node* find(int value) {
        // find the node containing value
        if (this->value == value) {
            return this;
        }
        else if (this->value > value) {
            if(left != nullptr){
                return left->find(value);
            }
        }
        else {
            if(left != nullptr){
                return right->find(value);
            }
        }
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
