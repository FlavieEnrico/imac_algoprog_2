#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort
    int min_idx;
    for (int i = 0; i <toSort.size(); i++)
    {
        min_idx = i;
        for (int j = i+1; j < toSort.size(); j++)
        if (toSort[j] < toSort[min_idx])
            min_idx = j;
        toSort.swap(min_idx, i);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
