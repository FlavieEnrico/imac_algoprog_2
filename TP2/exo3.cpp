#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
    int done=0;
    for (int i=0;i<toSort.size()-1;i++) {
        for (int j=0;j <toSort.size()-1;j++) {
            if(toSort[j]>toSort[j+1]) {
                toSort.swap(j, j+1);
                done++;
            }
        }
        if(done==0){
            break;
        }
        done=0;
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
